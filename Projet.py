

print("""
""")
print(
    """Utilisation des chaines de caractères (strings) avec différentes méthodes
""")

s1 ='Bonjour'
msg= s1.upper()
print("#1 Transformer la chaine de caractère en majuscule suivante :",s1,"en majuscule par la méthode upper")
print (msg
       )
print("""
""")


s2 = "je vous présente mon travail sur python"
majuscule = s2.capitalize()

print("#2 Mettre une majuscule sur la première lettre de la chaine de caractères avec la méthode capitalize")
print(majuscule)
print("""
""")

s3 = "Un cours Python a été dispensé au sein du Master SIEE'"
message=s3.endswith('SIEE')
print("#3 Identifier le dernier mot dans une chaine de caractère avec la méthode 'endswith'.")
print("Est ce que la phase suivante",s3,"termine avec le mot SIEE ?",message)
print("""
""")



s4="97900178916575145456"
digit=f"""s4: {s4}
{s4.isdigit()}
"""
print ("#3 Nous allons tester la méthode isdigit, qui permet de savoir si la chaine de caractère est un 'digit'")
print ("La chaine de caractères a testé est :",s4)
print ("La chaine de caractères est-elle un 'digit'?", digit)
print("""
""")

s5="9790,7"
decimal=f"""s5: {s5}
{s5.isdecimal()}
"""
print ("#4 Nous allons tester la méthode isdecimal, qui permet de savoir si la chaine de caractère est un décimal")
print ("La chaine de caractères a testé est :",s5)
print ("""La chaine de caractères est-elle un décimal?", decimal
""")


s6='Le master SIEE est en alternance'
find=s6.find('SIEE')
print(" #5 La méthode 'find' permet d'identifier la position d'une chaine de caractères donnée. La chaine de caractère est",s6,"On souhaite avoir le rang de 'SIEE'.")

print("La position de 'SIEE' est :",find)
print("""
""")


s7 = 'xyz\t12345\tabc'
table = s7.expandtabs()
print("#6 La méthode expandtabs permet de ranger la chaine de caractères en colonne")
print("La chaine de caractères initiale est :",s7)
print (table)
print("""
""")

s8="libérée, sauvée"
paroles=s8.replace("sauvée","délivrée")
print("#7 La méthode 'replace' permet de changer une succesion de chaines de caractères")
print("La chaine de caractères initiale est :",s8, "qui se transforme en :")
print(paroles)
print("""
""")

print("#9 La méthode 'lstrip' permet de supprimer les espaces")
s9 = '   hakuna matata '
disney=s9.lstrip()
print("Voici le résultat:",disney)

s10 = "SIEE"
count=s10.count('E')
print("#10Combien de fois il ya 'E' dans",s10,"?")
print(count)
print("""
""")

s11="ABCDEF"
swap1=s11.swapcase()
print("#11 La méthode 'swapcase' permet de mettre les caractères en minuscule lorsqu'elles sont en majuscule et vice versa")
print("La proposition initiale est:",s11)
print("Cela donne :",swap1, "en majusucle.")
s12="abcdef"
swap2=s12.swapcase()
print(swap2,"De minuscule à majuscule")
print("""
""")

s12="Fin de la partie sur les strings"
centrer=s2.center(15)
print(centrer)
print("""
""")

print("""Utilisation des listes avec différentes méthodes
""")

l1=['4167','820','2','997','82','0','820','4565','997','997']
countN=l1.count('997')
print("#1 Comptez dans la liste suivante",l1,"le nombre de répétition du nombre '997'.")
print("Le nombre '997' apparait",countN,"fois.")
print("""
""")

l2 = [2, 3, 5, 7, 9, 11]
l2.clear()
print("#2 La méthode 'clear' permet de supprimer tous les éléments d'une liste")
print('La liste initiale se transforme en', l2)
print("""
""")


l3= {'Physics':67, 'Maths':87}
copie = l3.copy()
print("#3La méthode 'copy' permet de copier une liste")
print("""la liste copiée est:""", copie)
print("""
""")

la=["voiture"];la+=[a**2 for a in range(5)]
lb=["bus"];lb+=[a**2 for a in (1,2,3,4,5)]
lc=["train"];lc+=[a**2 for a in range(2,7)]
l=[la]+[lb]+[lc]
print("#2 Ci-dessous la liste à ranger sous forme de table avec les colonnes suivantes 'voiture', 'bus' et 'train'")
print(l)
print("À l'aide de la méthode format, la liste se range de la manière suivante : ")

print("{}      {}      {}      {}      {}      {}".format(la[0],la[1],la[2],la[3],la[4],la[5]))
print("{}      {}      {}      {}     {}      {}".format(lb[0],lb[1],lb[2],lb[3],lb[4],lc[5]))
print("{}    {}      {}     {}     {}      {}".format(lc[0],lc[1],lc[2],lc[3],lc[4],lc[5]))
print("""
""")

li = ["girafe", "tigre"]
lii = ["singe", "souris"]
print("Les méthodes de calcul classiques sont applicables sur les listes")
agg= li+lii
produit=li*3
print("#3 Une somme de listes permet de d'aggréger plusieurs listes : ",li,"+",lii,"=",agg)

print("#4 Un produit de listes permet de répéter une liste n fois","3*",li,"=",produit)
print("""
""")


l5=["café", "thé","eau plate","eau gazeuse","vin","champagne","bière"]
length=len(l5)
print("#5 La méthode 'len' permet de compter la longueur de la liste")
print("La liste suivante :",l5, "a une longueur de",length,"chaines de caractères")
print("""
""")


nb_p = [2, 3, 5, 7,11]
removed_element = nb_p.pop(0)
print("#6 La méthode 'pop' permet d'effacer un élément de la liste (en fonction de sa position) et de le retourner sur une autre ligne")
print("La nouvelle liste regroupe les nombres premiers :",nb_p)
print("L'élément supprimé est : ", removed_element)
print("""
""")

print("#7 la méthode 'sort' permet de classer les éléments d'une liste dans l'ordre croissant")
l6= [11, 3, 7, 5, 2]
l6.sort()
print(l6)
print("""
""")

l7 = ['cat', 'dog', 'rabbit', 'horse']
index = l7.index('dog')
print("#8 La méthode 'index' permet de de retourner la position de l'élément choisi")
print("La position de l'élément 'dog' est :",index)
print("""
""")

l8 = ['a', 'e', 'i', 'u']
l8.insert(3, 'o')
print("#9 La méthode 'insert' permet d'ajouter un élément en précisant la position de l'élément à ajouter")
print("La liste des voyelles est la suite : ", l8)
print("""
""")

l9 = [3, 4, 5]
l10 = [1, 2]
l10.extend(l9)
print("#10 La méthode 'extend' permet de d'ajouter des éléments à une liste existante :", l10)
print("""
""")


l11=['a','b','c','d']
l11.reverse()
print("#11 La méthode 'reverse' permet d'inverser les positions des éléments de la liste")
print('la liste initiale devient :',l11)
print("""
""")


print(
    """Utilisation des dictionnaires avec différentes méthodes
""")


d1 = {"a": "123", "b": "456", "c": "789"}
row = "abc"
print(row.maketrans(d1))
print("""
""")




print("Utilisation de fonctions")


n = int(input("#1 Entrez un nombre: "))
if (n % 2) == 0:
   print("{0} est Paire".format(n))
else:
   print("{0} est Impaire".format(n))
   print ("""
""")


import string
import random

print("""Générateur de mot de passe
""")

## characters to generate password from
alphabets = list(string.ascii_letters)
digits = list(string.digits)
special_characters = list("!@#$%^&*()")
characters = list(string.ascii_letters + string.digits + "!@#$%^&*()")

def generate_random_password():
	## longueur du mot de passe 
	length = int(input("Quelle est la longueur du mot de passe souhaité: "))

	## conditions des caractères 
	alphabets_count = int(input("Combien de lettres voulez-vous ? : "))
	digits_count = int(input("Combien de chiffres voulez-vous ? : "))
	special_characters_count = int(input(" Combien de caractères spéciaux voulez-vous ?: "))

	characters_count = alphabets_count + digits_count + special_characters_count

	if characters_count > length:
		print("Le nombre de caractère est supérieur à la longueur du mot de passe. Veuillez saisir les conditions adéquates.")
		return generate_random_password()


	## Générer le mot de passe 
	password = []
	
	## Choisir des lettres alétoires
	for i in range(alphabets_count):
		password.append(random.choice(alphabets))


	## Choisir des chiffres aléatoires 
	for i in range(digits_count):
		password.append(random.choice(digits))


	## Choisir des lettres aléatoires 
	for i in range(special_characters_count):
		password.append(random.choice(special_characters))


	## Si le nombre de caractères est inférieur au nombre voulu par l'utilisateur 
	## Ajouter des caractères aléatoires 
	if characters_count < length:
		random.shuffle(characters)
		for i in range(length - characters_count):
			password.append(random.choice(characters))

	## Mélanger les caractères 
	random.shuffle(password)
	
	print("".join(password))



## invoking the function
generate_random_password()



print("""
""")
print("""Les paramètres
      """)
def identity(nom, age = 39, metier = "trader"):
    print("Mon nom est {}.\nJe suis {} et j'ai {} ans.".format(nom, metier, age))

identity("Picasso", 40, "peintre")
identity("Kerviel")


print (""" QCM mathématiques simples
""")
def menu():
   questions={'1+3= ':'4', '8/4= ':'2', '10-7= ':'3', '12/4= ':'3', ' 872920021*0= ':'0','7+29+100= ':'136'}
   print("Début du quiz\n")
   nom=input("Entrez votre nom  : ").title()
   print()
   print("\nBien joué {0}, vous avez repondu à {1} de {2} questions. ".format(nom, quiz(questions), len(questions)))

def quiz(qs): 
   points=0
   for qu,an in qs.items():
      if input(qu).lower()==an.lower():
         points+= 1
         print("Correct")
      else:
         print("Incorrect, la bonne réponse était \"{}\".".format(an))
         return points

if __name__=="__main__":
      menu()

print("FIN")
      
